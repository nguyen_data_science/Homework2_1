#Given a dictionary my_dict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}.
# Write code to print out a list of sorted key based on their value.
# For example, in this case, the code should print out ['b', 'd', 'a', 'c']
d={'a': 9, 'b': 1, 'c': 12, 'd': 7}
v=list(d.values())
k=list(d.keys())
sort=sorted(range(len(v)), key=lambda k: v[k])
e=[1,2,3,4]
for i in range(4):
    e[i]=k[sort[i]]
print(e)