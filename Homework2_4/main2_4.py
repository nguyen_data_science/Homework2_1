#Take two lists, say for example these two:

#a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

# and b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

# write a program that returns a list that contains only the elements that are common between
# the lists (without duplicates). Make sure your program works on two lists of different sizes.
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
a_1=set(a)
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
b_1=set(b)
print(a_1.intersection(b_1))