#Write a function that accept a string as a single argument and print out whether that string is a palindrome.
#  (A palindrome is a string that reads the same forwards and backwards.) For example, "abcddcba" is a palindrome,
#  "1221" is also a palindrome.
x="12521"
y="abccba"
z="abcd"
def check_palind(x):
    k=True
    for i in range(len(x)//2):
        if x[i]!=x[-1-i]:
            k=False
            break
    return(k)
print(check_palind(x))
print(check_palind(y))
print(check_palind(z))